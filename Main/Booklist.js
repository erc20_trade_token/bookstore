import React, { useState } from 'react';
import { View, Text, FlatList, TouchableOpacity, Modal, Button, StyleSheet } from 'react-native';
import bookData from "../BookDatas/Bookdata";

const BookList = () => {
  const [selectedBook, setSelectedBook] = useState(null);
  const [isModalVisible, setIsModalVisible] = useState(false);

  const renderItem = ({ item }) => {
    return (
      <TouchableOpacity 
        onPress={() => { 
          setSelectedBook(item); 
          setIsModalVisible(true);
        }} 
        style={styles.itemContainer}
      > 
        <View style={styles.bookContainer}>
          <Text style={styles.title}>{item.title}</Text> 
          <Text style={styles.author}>{item.author}</Text> 
          <Text style={styles.price}>{item.price}</Text> 
          <Button title="Read" onPress={() => { console.log('Read button pressed'); }} />
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <FlatList 
        data={bookData} 
        keyExtractor={(item) => item.id} 
        renderItem={renderItem} 
        numColumns={2}
        key={isModalVisible ? 'modal' : 'list'}
        style={styles.list}
      />
      <Modal 
        visible={isModalVisible} 
        animationType="slide" 
        transparent={true} 
      > 
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <Text style={styles.modalTitle}>{selectedBook ? selectedBook.title : ''}</Text> 
            <Text style={styles.modalAuthor}>{selectedBook ? selectedBook.author : ''}</Text> 
            <Text style={styles.modalPrice}>{selectedBook ? selectedBook.price : ''}</Text> 
            <Button title="Close" onPress={() => { setIsModalVisible(false); setSelectedBook(null); }} />
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2',
    paddingTop: 16,
  },
  list: {
    paddingHorizontal: 16,
  },
  itemContainer: {
    width: '50%',
    padding: 8,
  },
  bookContainer: {
    backgroundColor: 'white',
    padding: 16,
    borderRadius: 8,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  title: {
    fontSize: 18, 
    fontWeight: 'bold',
  },
  author: {
    fontSize: 16,
  },
  price: {
    fontSize: 16, 
    color: 'green',
  },
  modalContainer: {
    flex: 1, 
    justifyContent: 'center', 
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalContent: {
    backgroundColor: 'white', 
    padding: 16, 
    borderRadius: 8, 
    width: '80%',
  },
  modalTitle: {
    fontSize: 24, 
    fontWeight: 'bold',
    marginBottom: 8,
  },
  modalAuthor: {
    fontSize: 20,
    marginBottom: 8,
  },
  modalPrice: {
    fontSize: 20, 
    color: 'green',
    marginBottom: 16,
  },
});

export default BookList;