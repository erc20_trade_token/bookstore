const bookData = [
    {
      id: '1',
      title: 'To Kill a Mockingbird',
      author: 'Harper Lee',
      price: 'Nu:500',
    },
    {
      id: '2',
      title: '984',
      author: 'George Orwell',
      price: 'Nu:300',
    },
    {
        id: '3',
        title: 'The Great Gatsby',
        author: 'F. Scott Fitzgerald',
        price: 'Nu:600',
      },
      {
        id: '4',
        title: 'Pride and Prejudice',
        author: 'Jane Austen',
        price: 'Nu:230',
      },
      {
        id: '5',
        title: 'One Hundred Years of Solitude',
        author: 'abriel García Márquez',
        price: 'Nu:290',
      },
      {
        id: '6',
        title: 'The Catcher in the Rye',
        author: 'J.D. Salinge',
        price: 'Nu:650',
      },
  ];
  export default bookData;